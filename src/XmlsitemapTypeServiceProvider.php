<?php

namespace Drupal\xmlsitemap_type;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Reregister 'xmlsitemap_generator' to the module service.
 *
 * Please be aware of the class name
 * {@link https://www.drupal.org/docs/8/api/services-and-dependency-injection/altering-existing-services-providing-dynamic-services}
 */
class XmlsitemapTypeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('xmlsitemap_generator')) {
      $definition = $container->getDefinition('xmlsitemap_generator');
      $definition->setClass('Drupal\xmlsitemap_type\XmlSitemapTypeGenerator')
        ->addArgument(new Reference('database'))
        ->addArgument(new Reference('path.alias_storage'));
    }
  }

}
