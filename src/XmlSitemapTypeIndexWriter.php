<?php

namespace Drupal\xmlsitemap_type;

use Drupal\Core\Url;
use Drupal\xmlsitemap\XmlSitemapIndexWriter;
use Drupal\xmlsitemap\XmlSitemapInterface;

/**
 * Override generation of the sitemap index.
 */
class XmlSitemapTypeIndexWriter extends XmlSitemapIndexWriter {

  /**
   * @var \Drupal\xmlsitemap_type\TypeChunk[]
   */
  protected $typeChunks;

  /**
   * XmlSitemapTypeIndexWriter constructor.
   *
   * @param \Drupal\xmlsitemap\XmlSitemapInterface $sitemap
   *   The sitemap config entity.
   * @param \Drupal\xmlsitemap_type\TypeChunk[] $type_chunks
   *   A list of type chunks.
   */
  public function __construct(XmlSitemapInterface $sitemap, $type_chunks) {
    parent::__construct($sitemap, 'index');
    $this->typeChunks = $type_chunks;
  }

  /**
   * {@inheritdoc}
   */
  public function generateXML() {
    // @codingStandardsIgnoreEnd
    $lastmod_format = \Drupal::config('xmlsitemap.settings')->get('lastmod_format');

    $path = \Drupal::service('path.alias_storage');
    $lang = \Drupal::languageManager()->getDefaultLanguage();

    $url_options = $this->sitemap->uri['options'];
    $url_options += [
      'absolute' => TRUE,
      'xmlsitemap_base_url' => \Drupal::state()->get('xmlsitemap_base_url'),
      'language' => $lang,
      'alias' => TRUE,
    ];
    $base_url = rtrim(\Drupal::state()->get('xmlsitemap_base_url'), '/');

    foreach ($this->typeChunks as $typeChunk) {
      for ($i = 1, $chunks = $typeChunk->chunk; $i <= $chunks; $i++) {
        $typeChunk->chunk = $i;
        $filename = $typeChunk->getName() . '.xml';
        $alias = '/' . $filename;
        $source = Url::fromRoute('xmlsitemap_type.sitemap_chunk', [
          'filename' => $filename,
        ])->toString();
        $path->save($source, $alias, $lang->getId());
//        $url = Url::fromRoute('xmlsitemap_type.sitemap_chunk', [
//          'filename' => $filename,
//        ], $url_options)->toString();
        $url = $base_url . '/' . $filename;
        $element = [
          'loc' => $url,
          'lastmod' => empty($typeChunk->getLastMod($i)) ? '' : gmdate($lastmod_format, $typeChunk->getLastMod($i)),
        ];
        $this->writeSitemapElement('sitemap', $element);
      }
    }
  }

}