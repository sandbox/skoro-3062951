<?php

namespace Drupal\xmlsitemap_type;

/**
 * Sitemap type chunk.
 *
 * The type chunk is a part of the sitemap that contains only links
 * related to only specific type and subtype.
 */
class TypeChunk {

  /**
   * Entity types.
   *
   * The array indexed by entity type name and contains a list of
   * the entity bundles.
   *
   * @var array
   */
  public $type = [];

  /**
   * A chunk number.
   *
   * @var int
   */
  public $chunk = 1;

  /**
   * Last modification query.
   *
   * The value in hours. Adds condition for query only entities with
   * the latest modification date.
   *
   * @var int
   */
  public $period = 0;

  /**
   * Is it a single chunk ?
   *
   * @var bool
   */
  public $isSingle = FALSE;

  /**
   * The chunks last modification date.
   *
   * @var array
   *   A list of dates for chunks.
   */
  public $lastMod = [];

  /**
   * The type chunk name.
   *
   * @var string
   */
  protected $name;

  /**
   * TypeChunk constructor.
   *
   * @param string $name
   *   The type chunk name.
   */
  public function __construct(string $name = '') {
    $this->name = $name;
  }

  /**
   * Sets the type chunk name.
   *
   * @param string $name
   *   The type chunk name.
   *
   * @return $this
   */
  public function setName(string $name) {
    $this->name = $name;
    return $this;
  }

  /**
   * Returns type chunk original name (without chunk number).
   *
   * @return string
   *   The chunk name.
   */
  public function getBaseName(): string {
    return $this->name;
  }

  /**
   * Returns a current chunk modification date.
   *
   * @return int
   *   The unix time.
   */
  public function getCurrentLastMod(): int {
    return $this->lastMod[$this->chunk] ?? 0;
  }

  /**
   * Returns the modification date for the specified chunk.
   *
   * @param int $chunk
   *   The chunk.
   *
   * @return int
   *   The unix time.
   */
  public function getLastMod($chunk): int {
    return $this->lastMod[$chunk] ?? 0;
  }

  /**
   * Sets the modification date for the current chunk.
   *
   * @param int $lastmod
   *   The unix time.
   *
   * @return $this
   */
  public function setCurrentLastMod($lastmod) {
    $this->lastMod[$this->chunk] = $lastmod;
    return $this;
  }

  /**
   * Returns a type chunk name.
   *
   * @return string
   *   The type chunk name.
   */
  public function getName(): string {
    return $this->name . ($this->isSingle ? '' : $this->chunk);
  }

  /**
   * Converts the object to a string.
   *
   * @return string
   *   The type chunk name.
   *
   * @see \Drupal\xmlsitemap_type\TypeChunk::getName()
   */
  public function __toString() {
    return $this->getName();
  }

}