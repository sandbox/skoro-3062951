<?php

namespace Drupal\xmlsitemap_type\Controller;

use Drupal\Core\Url;
use Drupal\xmlsitemap\Controller\XmlSitemapController;
use Drupal\xmlsitemap\Entity\XmlSitemap;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Xml sitemap controller.
 *
 * Provides routes for sitemap.xml and sitemap xml chunks.
 */
class XmlSitemapTypeController extends XmlSitemapController {

  /**
   * Gets the xml sitemap by a context.
   *
   * @return \Drupal\xmlsitemap\Entity\XmlSitemap
   *   The xml sitemap entity.
   */
  protected function getSitemap(): XmlSitemap {
    $sitemap = XmlSitemap::loadByContext();
    if (!$sitemap) {
      throw new NotFoundHttpException();
    }
    return $sitemap;
  }

  /**
   * Provides the sitemap in XML format.
   *
   * @param string $filename
   *   A sitemap chunk name.
   *
   * @throws NotFoundHttpException
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The sitemap in XML format or plain text if xmlsitemap_developer_mode flag
   *   is set.
   */
  public function renderSitemapTypeXml(string $filename) {
    $sitemap = $this->getSitemap();
    if (($p = strrpos($filename, '.')) === FALSE) {
      throw new NotFoundHttpException();
    }
    $chunk = substr($filename, 0, $p);
    $file = xmlsitemap_sitemap_get_file($sitemap, $chunk);
    $response = new Response();
    return xmlsitemap_output_file($response, $file);
  }

  /**
   * The index sitemap.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The index sitemap.
   */
  public function renderSitemapIndexXml(Request $request) {
    $path = $request->getPathInfo();
    if ($path == '/sitemap.xml') {
      $index = Url::fromRoute('xmlsitemap_type.sitemap_index', [], [
        'absolute' => TRUE,
      ])->toString();
      return new RedirectResponse($index);
    }

    $sitemap = $this->getSitemap();
    $file = xmlsitemap_sitemap_get_file($sitemap, 'index');
    $response = new Response();

    return xmlsitemap_output_file($response, $file);
  }

}
