<?php

namespace Drupal\xmlsitemap_type;

use Drupal\Core\Database\Connection;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\xmlsitemap\Entity\XmlSitemap;
use Drupal\xmlsitemap\XmlSitemapGenerator;
use Drupal\xmlsitemap\XmlSitemapInterface;
use Drupal\xmlsitemap\XmlSitemapWriter;
use Psr\Log\LoggerInterface;

/**
 * Xml sitemap generator.
 *
 * Generates sitemaps splited by type of content.
 */
class XmlSitemapTypeGenerator extends XmlSitemapGenerator {

  const STATE_TYPECHUNKS = 'xmlsitemap_typechunks';

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * Constructs a XmlSitemapGenerator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Path\AliasStorageInterface $aliasStorage
   *   The alias storage service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    LanguageManagerInterface $language_manager,
    LoggerInterface $logger,
    ModuleHandlerInterface $module_handler,
    Connection $connection,
    AliasStorageInterface $aliasStorage
  ) {
    parent::__construct($config_factory, $state, $language_manager, $logger, $module_handler);
    $this->connection = $connection;
    $this->aliasStorage = $aliasStorage;
  }

  /**
   * Returns a list of configured type chunks.
   *
   * @return \Drupal\xmlsitemap_type\TypeChunk[]
   *   The list of type chunks.
   */
  public function getTypeChunks() {
    $chunks = [];
    $maps = xmlsitemap_type_config_maps();
    foreach ($maps as $map_name => $params) {
      $type_chunk = new TypeChunk($map_name);
      if (isset($params['period'])) {
        $type_chunk->period = $params['period'];
      }
      foreach ($params['entity'] as $entity_name => $bundles) {
        $type_chunk->type[$entity_name] = $bundles;
      }
      $chunks[] = $type_chunk;
    }
    return $chunks;
  }

  /**
   * {@inheritdoc}
   */
  public function regenerateBefore() {
    parent::regenerateBefore();
    $query = $this->connection->select('url_alias', 'u');
    $query->fields('u', ['pid']);
    $query->condition('source', '/sitemap/%.xml', 'LIKE');
    $rows = $query->execute();
    while ($row = $rows->fetch()) {
      $this->aliasStorage->delete(['pid' => $row->pid]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function regenerateBatchGenerateIndex($smid, &$context) {
    $sitemap = xmlsitemap_sitemap_load($smid);
    $type_chunks = $this->state->get(self::STATE_TYPECHUNKS);
    if (!$type_chunks) {
      $type_chunks = $this->getTypeChunks();
    }
    $this->generateTypeIndex($sitemap, $type_chunks);
    $context['message'] = t('Now generating sitemap index %sitemap-url.', [
      '%sitemap-url' => Url::fromRoute('xmlsitemap.sitemap_xml', [], $sitemap->uri['options'])->toString(),
    ]);
  }

  /**
   * Generates the sitemap index.
   *
   * @param \Drupal\xmlsitemap\XmlSitemapInterface $sitemap
   *   The sitemap entity.
   * @param \Drupal\xmlsitemap_type\TypeChunk[] $type_chunks
   *   The list of type chunks.
   *
   * @return int
   *   The number of elements in the index.
   *
   * @throws \Drupal\xmlsitemap\XmlSitemapGenerationException
   */
  public function generateTypeIndex(XmlSitemapInterface $sitemap, $type_chunks) {
    try {
      $writer = new XmlSitemapTypeIndexWriter($sitemap, $type_chunks);
      $writer->startDocument();
      $writer->generateXML();
      $writer->endDocument();
    }
    catch (Exception $e) {
      $this->logger->error($e);
      throw $e;
    }

    return $writer->getSitemapElementCount();
  }

  /**
   * Generates the chunk page.
   *
   * @param \Drupal\xmlsitemap\XmlSitemapInterface $sitemap
   *   The sitemap entity.
   * @param \Drupal\xmlsitemap_type\TypeChunk $type_chunk
   *   The type chunk.
   *
   * @return int
   *   The number of elements in the page.
   *
   * @throws \Drupal\xmlsitemap\XmlSitemapGenerationException
   */
  public function generateTypePage(XmlSitemapInterface $sitemap, TypeChunk $type_chunk) {
    try {
      $writer = new XmlSitemapTypeWriter($sitemap, $type_chunk, $this->moduleHandler);
      $writer->startDocument();
      $links = $this->generateTypeChunk($sitemap, $writer, $type_chunk);
      $this->moduleHandler->invokeAll('xmlsitemap_type_page', [$writer, $type_chunk, $sitemap]);
      $links = $writer->getSitemapElementCount();
      if ($links == 0) {
        file_unmanaged_delete($writer->getURI());
      }
      else {
        $writer->endDocument();
      }
    }
    catch (Exception $e) {
      $this->logger->error($e);
      throw $e;
    }

    return $links;
  }

  /**
   * Chunk generator.
   *
   * @param \Drupal\xmlsitemap\XmlSitemapInterface $sitemap
   *   The sitemap entity.
   * @param \Drupal\xmlsitemap\XmlSitemapWriter $writer
   *   The type chunk writer.
   * @param \Drupal\xmlsitemap_type\TypeChunk $type_chunk
   *   The type chunk.
   *
   * @return int
   *   The count of elements in the chunk.
   */
  public function generateTypeChunk(XmlSitemapInterface $sitemap, XmlSitemapWriter $writer, TypeChunk $type_chunk) {
    $lastmod_format = $this->config->get('lastmod_format');

    $url_options = $sitemap->uri['options'];
    $url_options += [
      'absolute' => TRUE,
      'base_url' => rtrim($this->state->get('xmlsitemap_base_url'), '/'),
      'language' => $this->languageManager->getDefaultLanguage(),
      // @todo Figure out a way to bring back the alias preloading optimization.
      // 'alias' => $this->config->get('prefetch_aliases'),
      'alias' => FALSE,
    ];

    $last_url = '';
    $link_count = 0;

    $query = $this->connection->select('xmlsitemap', 'x');
    $query->fields('x', [
      'id', 'loc', 'type', 'subtype', 'lastmod', 'changefreq', 'changecount', 'priority', 'language', 'access', 'status',
    ]);
    $query->condition('x.access', 1);
    $query->condition('x.status', 1);
    foreach ($type_chunk->type as $type => $bundles) {
      $query->condition('x.type', $type);
      $query->condition('x.subtype', $bundles, 'IN');
    }
    if (!empty($type_chunk->period)) {
      $time = strtotime("- {$type_chunk->period} hours");
      $query->condition('x.lastmod', $time, '>');
    }

    $query->orderBy('x.lastmod', 'DESC');
    $query->orderBy('x.language', 'DESC');
    $query->orderBy('x.loc');
    $query->addTag('xmlsitemap_generate');
    $query->addMetaData('sitemap', $sitemap);

    $offset = max($type_chunk->chunk - 1, 0) * xmlsitemap_get_chunk_size();
    $limit = xmlsitemap_get_chunk_size();
    $query->range($offset, $limit);
    $links = $query->execute();

    while ($link = $links->fetchAssoc()) {
      $link['language'] = $link['language'] != LanguageInterface::LANGCODE_NOT_SPECIFIED ? xmlsitemap_language_load($link['language']) : $url_options['language'];
      $link_options = [
        'language' => $link['language'],
        'xmlsitemap_link' => $link,
        'xmlsitemap_sitemap' => $sitemap,
      ];
      // @todo Add a separate hook_xmlsitemap_link_url_alter() here?
      $link['loc'] = empty($link['loc']) ? '/' : $link['loc'];
      $link_url = Url::fromUri('internal:' . $link['loc'], $link_options + $url_options)->toString();

      // Update lastmod date of type chunk.
      if ((int) $link['lastmod'] > $type_chunk->getCurrentLastMod()) {
        $type_chunk->setCurrentLastMod((int) $link['lastmod']);
      }

      // Skip this link if it was a duplicate of the last one.
      // @todo Figure out a way to do this before generation so we can report
      // back to the user about this.
      if ($link_url == $last_url) {
        continue;
      }
      else {
        $last_url = $link_url;
        // Keep track of the total number of links written.
        $link_count++;
      }

      $element = [];
      $element['loc'] = $link_url;
      if ($link['lastmod']) {
        $element['lastmod'] = gmdate($lastmod_format, $link['lastmod']);
        // If the link has a lastmod value, update the changefreq so that links
        // with a short changefreq but updated two years ago show decay.
        // We use abs() here just incase items were created on this same cron
        // run because lastmod would be greater than REQUEST_TIME.
        $link['changefreq'] = (abs(REQUEST_TIME - $link['lastmod']) + $link['changefreq']) / 2;
      }
      if ($link['changefreq']) {
        $element['changefreq'] = xmlsitemap_get_changefreq($link['changefreq']);
      }
      if (isset($link['priority']) && $link['priority'] != 0.5) {
        // Don't output the priority value for links that have 0.5 priority.
        // This is the default 'assumed' value if priority is not included as
        // per the sitemaps.org specification.
        $element['priority'] = number_format($link['priority'], 1);
      }

      // @todo Should this be moved to XMLSitemapWriter::writeSitemapElement()?
      $link['type_chunk'] = $type_chunk;
      $this->moduleHandler->alter('xmlsitemap_element', $element, $link, $sitemap);
      // alter hook can skip the link by nulling an element.
      if (!empty($element)) {
        $writer->writeSitemapElement('url', $element);
      }

    }

    return $link_count;
  }

  /**
   * {@inheritdoc}
   */
  public function regenerateBatchGenerate($smid, array &$context) {
    if (!isset($context['sandbox']['sitemap'])) {
      $sitemap = xmlsitemap_sitemap_load($smid);
      $context['sandbox']['sitemap'] = $sitemap;
      $context['sandbox']['sitemap']->setChunks(1);
      $context['sandbox']['sitemap']->setLinks(0);
      $context['sandbox']['max'] = XMLSITEMAP_MAX_SITEMAP_LINKS;
      $context['sandbox']['chunks'] = $this->getTypeChunks();

      // Clear the cache directory for this sitemap before generating any files.
      xmlsitemap_check_directory($context['sandbox']['sitemap']);
      xmlsitemap_clear_directory($context['sandbox']['sitemap']);
      $this->state->delete(self::STATE_TYPECHUNKS);
    }
    $sitemap = &$context['sandbox']['sitemap'];
    $chunks = $sitemap->getChunks();
    $type_chunk = $context['sandbox']['chunks'][$chunks - 1] ?? NULL;
    $links = 0;
    if ($type_chunk) {
      $links = $this->generateTypePage($sitemap, $type_chunk);
      $context['message'] = t('Now generating %sitemap-url.', [
        '%sitemap-url' => Url::fromRoute('xmlsitemap_type.sitemap_chunk', [
          'filename' => $type_chunk->getName() . '.xml',
        ], $sitemap->uri['options'])->toString(),
      ]);
    }

    if ($links) {
      $sitemap->setLinks($sitemap->getLinks() + $links);
      $type_chunk->chunk++;
    }
    elseif ($chunks >= count($context['sandbox']['chunks'])) {
      $this->normalizeChunk($sitemap, $type_chunk);
      $sitemap->setChunks($sitemap->getChunks() - 1);

      // Save the updated chunks and links values.
      $context['sandbox']['max'] = $sitemap->getChunks();
      $sitemap->setUpdated(REQUEST_TIME);
      xmlsitemap_sitemap_get_max_filesize($sitemap);
      xmlsitemap_sitemap_save($sitemap);

      $this->state->set(self::STATE_TYPECHUNKS, $context['sandbox']['chunks']);
    }
    else {
      $this->normalizeChunk($sitemap, $type_chunk);
      $sitemap->setChunks($sitemap->getChunks() + 1);
    }

    if ($sitemap->getChunks() != $context['sandbox']['max']) {
      $context['finished'] = $sitemap->getChunks() / $context['sandbox']['max'];
    }
  }

  /**
   * Normalizes a chunk name.
   *
   * @param \Drupal\xmlsitemap\Entity\XmlSitemap $sitemap
   *   The sitemap entity.
   * @param \Drupal\xmlsitemap_type\TypeChunk $typeChunk
   *   The type chunk.
   */
  protected function normalizeChunk(XmlSitemap $sitemap, TypeChunk $typeChunk) {
    $typeChunk->chunk--;
    if ($typeChunk->chunk == 1) {
      $old_uri = xmlsitemap_sitemap_get_file($sitemap, $typeChunk->getName());
      $typeChunk->isSingle = TRUE;
      $new_uri = xmlsitemap_sitemap_get_file($sitemap, $typeChunk->getName());
      file_unmanaged_move($old_uri, $new_uri);
    }
  }

}
