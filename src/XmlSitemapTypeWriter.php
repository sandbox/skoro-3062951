<?php

namespace Drupal\xmlsitemap_type;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\xmlsitemap\XmlSitemapInterface;
use Drupal\xmlsitemap\XmlSitemapWriter;

/**
 * Overrides the sitemap chunk names.
 */
class XmlSitemapTypeWriter extends XmlSitemapWriter {

  /**
   * @var \Drupal\xmlsitemap_type\TypeChunk
   */
  protected $typeChunk;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * XmlSitemapTypeWriter constructor.
   *
   * @param \Drupal\xmlsitemap\XmlSitemapInterface $sitemap
   *   The sitemap entity.
   * @param \Drupal\xmlsitemap_type\TypeChunk $type_chunk
   *   The type chunk instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(XmlSitemapInterface $sitemap, TypeChunk $type_chunk, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($sitemap, $type_chunk->getName());
    $this->typeChunk = $type_chunk;
    $this->sitemap_page = $type_chunk->chunk;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getRootAttributes() {
    $attributes['xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    if (\Drupal::state()->get('xmlsitemap_developer_mode')) {
      $attributes['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';
      $attributes['xsi:schemaLocation'] = 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd';
    }

    $this->moduleHandler->alter('xmlsitemap_type_root_attributes', $attributes, $this->sitemap, $this->typeChunk);

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function writeElement($name, $content = '') {
    if ($name == 'news:images') {
      foreach ($content as $image) {
        $this->writeElement('image:image', $image);
      }
    }
    elseif (is_array($content)) {
      $this->startElement($name);
      foreach ($content as $sub_name => $sub_content) {
        $this->writeElement($sub_name, $sub_content);
      }
      $this->endElement();
    }
    elseif(is_object($content)) {
      parent::writeElement($name, $content->toString());
    }
    else {
      parent::writeElement($name, $content);
    }
  }

}