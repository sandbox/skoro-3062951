<?php

namespace Drupal\xmlsitemap_type\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if (($route = $collection->get('xmlsitemap.sitemap_xml'))) {
      $route->setDefault('_controller', 'Drupal\xmlsitemap_type\Controller\XmlSitemapTypeController::renderSitemapIndexXml');
    }
  }

}